# -*- coding: utf-8 -*-
import matplotlib.colors as clr
import matplotlib
import matplotlib.animation as anim
import matplotlib.pyplot as pl

#this file is to be only executed on a scope where a solution of a Fokker-Planck dynamic is already stored in a list named "frames" 

matplotlib.use('Agg')
Writer = anim.writers['ffmpeg']
writer = Writer(fps=10, metadata=dict(artist='Me'), bitrate=1800)


fig = pl.figure(figsize=(11,5)); axes = fig.add_subplot(111)
pl.contour(V, colors='white',origin='lower',extent=[-Lx,Lx,-Ly,Ly],levels=[Vm/2,Vm/4,Vm/10,Vm/20])
line= pl.imshow(frames[0],norm=clr.Normalize(vmin=0.0,vmax=np.max(frames[0])), animated=True, extent=[-Lx/2,Lx/2,-Ly/2,Ly/2]) #LogNorm(vmin=0.0,vmax=1.0, clip=True)

#fig.suptitle(r'$v=$'+str(v)+' '+r'$a =$'+str(v)+' '+r'$\mu=$'+str(mu)+' '+r'$D=$'+str(D), fontsize=12)
mu=0
fig.suptitle('v='+str(v)+' '+'a ='+str(a)+' '+'D=$'+str(D), fontsize=12)


def plot_frame(i):
    line.set_array(frames[i])
    line.set_norm(norm=clr.Normalize(vmin=0.0,vmax=np.max(frames[i])))
    t=str(Time[i])
    if Time[i]<0.1:
        axes.set_title('t='+t[0:7]+'s')
    elif Time[i]<1:
        axes.set_title('t='+t[0:5]+'s')
    elif Time[i]<10:
        axes.set_title('t='+t[0:3]+'s')
    else:
        k=int(np.log10(Time[i]))+1
        axes.set_title('t='+t[0:k]+'s')
    #cb=fig.colorbar(line)
    #cb.ax.set_ylabel('P')
    fig.canvas.draw()
    return fig
# Animate the solution
animation=anim.FuncAnimation(fig, plot_frame, frames=len(Time), interval=100, repeat=False)

#do not forget to edit the title or a previous video may be erased

animation.save("FK_vidtet_new2.mp4", writer=writer)