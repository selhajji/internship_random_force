import numpy as np
import autograd.numpy as npa
import scipy.spatial as sp
from scipy.linalg import cholesky, cho_solve
from numpy import fft as f
from autograd import grad, jacobian
from autograd import elementwise_grad as egrad

##
# parameters
d=2

#dx=0.02
L=1
N=32
dx=L/N
#N=L/dx

mu=0

l_cV=0.3
l_cA=0.3



alpha_A=1
alpha_V=1

e=0.2
v=e
a=1-e

def gamma_V_func(r):
    return v**2*np.exp(-1/2*(r/l_cV)**(2*alpha_V))
def gamma_A_func(r):
    return a**2*np.exp(-1/2*(r/l_cA)**(2*alpha_A))

xi,eta=f.rfftfreq(2*N)*(2*N)*2*np.pi/(2*L), f.fftfreq(2*N)*2*N*2*np.pi/(2*L)
xi, eta=np.meshgrid(xi,eta)
k2=xi**2+eta**2

X= np.mgrid[tuple(slice(- L, L, dx) for _ in range(d))].T

Y=X.reshape((-1,d))
I=np.ones_like(Y.sum(axis=-1))
dist=sp.distance_matrix(Y,Y,p=2)


covA=gamma_A_func(dist)

eps=np.identity(len(Y))*10**-12


#f.rfft2(np.random.standard_normal(X[:,:,0].shape))
def make_V(d=d,ampl=v,l_c=l_cV,k2=k2,N=N,dist=dist,eps=eps):
    covV=gamma_V_func(dist)
    L=cholesky(covV+eps,lower=True)
    Vh=(2*N)**d*ampl*(2*np.pi*l_c**2)**(d/2)*np.exp(-l_c**2*k2/4)*np.random.standard_normal(k2.shape)
    V=1/2*f.irfft2(Vh)
    V2=V.reshape(Y[:,0].shape)
    KV=cho_solve((L,True),V2)
    return V,KV
def make_A(d=d,ampl=a,l_c=l_cA,k2=k2,N=N,dist=dist,eps=eps):
    covA=gamma_A_func(dist)
    L=cholesky(covA+eps,lower=True)
    A=np.zeros((d,d)+(2*N,2*N))
    KA=np.zeros((d,d,(2*N)**d))
    for i in range(1,d):
        for j in range(i):
            AH=(2*N)**d*ampl*(2*np.pi*l_c**2)**(d/2)*np.exp(-l_c**2*k2/4)*np.random.standard_normal(k2.shape)
            A[i,j,:]=1/2*f.irfft2(AH)
            A[j,i,:]=-A[i,j,:]
            A2=A[i,j,:].reshape(Y[:,0].shape)
            KA[i,j,:]=cho_solve((L,True),A2)
            KA[j,i,:]=-KA[i,j,:]
        return A,KA

V,KV=make_V()
A,KA=make_A()
def Gamma_VX(x,Y):
    x=x.reshape((-1,1,x.shape[-1]))
    r=npa.sqrt(((Y-x)**2).sum(axis=-1))
    return v**2*npa.exp(-1/2*(r/l_cV)**(2*alpha_V))

def Gamma_AX(x,Y):
    x=x.reshape((-1,1,x.shape[-1]))
    r=npa.sqrt(((Y-x)**2).sum(axis=-1))
    return a**2*npa.exp(-(r/l_cA)**alpha_A)

#potential and vector potential functions ready to use

def V_func(x,Y=Y):
    return npa.dot(Gamma_VX(x,Y),KV)

def A_func(x,Y=Y):
    return npa.tensordot(Gamma_AX(x,Y),KA,[[1],[2]])

## generate force

nablaV=egrad(V_func,argnum=0)


coordA={}
for i in range(1,d):
    for j in range(i):
        def A_coord(x,Y=Y):
            AT=npa.tensordot(Gamma_AX(x,Y),KA,[[1],[2]]).T
            return AT[i,j,:]
        coordA[(i,j)]=egrad(A_coord)
def jacobA(x,coordA=coordA):
    JA=np.zeros((d,d)+x.shape)
    for i in range(1,d):
        for j in range(i):
            JA[i,j,:]=coordA[(i,j)](x)
            JA[j,i,:]=-JA[i,j,:]
    return JA

def rotA(x):
    jacA=jacobA(x)*1/np.sqrt(2)
    return np.einsum('ij...j -> ...i',jacA)



